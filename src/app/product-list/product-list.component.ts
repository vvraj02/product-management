import { ProductServiceService } from '../services/product-service.service';
import { ProductApisService } from '../services/product-apis.service'
import { GlobalUrls } from './../Utils/GlobalUrl';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DeleteProductComponent } from '../popup/delete-product/delete-product.component';
import { Constant } from '../models/constant.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, Sort } from '@angular/material/sort';
import { LiveAnnouncer } from '@angular/cdk/a11y';
@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  isDisable = false;
  productData: any[];
  displayedColumns = [];
  dataSource = new MatTableDataSource<any>();
  selectedValue: string;
  productCateoryList: any[] = [...Constant.productCateoryList];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private router: Router, private productApisService: ProductApisService, private productService: ProductServiceService, public dialog: MatDialog, private _liveAnnouncer: LiveAnnouncer) {
  }

  ngOnInit() {
    // with using api call
    // this.getAllProducts();

    //with using localstorage 
    this.productCateoryList.splice(0, 0, { key: 'all', value: 'All' });
    this.productData = this.productService.getProductData();
    if (this.productService.getLocalstorageData("selectCategory")) {
      this.selectedValue = this.productService.getLocalstorageData("selectCategory");
      this.sortOnCategoryBase(this.selectedValue);
    }
    this.displayedColumns = Object.keys(this.productData[0]);
    this.displayedColumns.push('action');
    this.dataSource = new MatTableDataSource<any>(this.productData);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  updateProductData(singleProduct) {
    this.router.navigate(['/update-product', singleProduct.id]);
  }

  navigateAddProduct() {
    this.productService.deleteLocalstorageData('updateProduct');
    this.router.navigate(['/add-product']);
  }

  openDialog(product) {
    const popupRef = this.dialog.open(DeleteProductComponent, {
      data: {
        msg: `Are you sure, You want to delete this ${product.productName} ?`
      }
    });
    popupRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.deleteProduct(product);
      }
    });
  }

  deleteProduct(product) {
    // using localstorage
    this.productData = this.productService.getProductData();
    const productIndex = this.productData.findIndex(value => value.id === product.id);
    this.productData.splice(productIndex, 1);
    this.productData = [...this.productData];
    this.productService.setLocalstorageData('productData', this.productData);
    this.productService.openSnackBar("Sucess", `Deleted product ${product.productName} sucessfully`);
    this.sortOnCategoryBase(this.selectedValue);

    // using apis
    // this.productApisService.deletProduct(GlobalUrls.deleteProductUrl.replace('{id}', product.id)).subscribe(response => {
    //   console.log(response);
    //   this.productService.openSnackBar("Sucess", `Deleted product ${product.productName} sucessfully`);
    //   this.getAllProducts();
    // }, error => {
    //   console.log(error);
    // });
  }

  getAllProducts() {
    this.productApisService.callGetApi(GlobalUrls.getAllProductsUrl).subscribe((response: any) => {
      this.productData = response.result.list;
      this.productService.setLocalstorageData('productData', this.productData);
      if (this.productService.getLocalstorageData('selectCategory')) {
        this.selectedValue = this.productService.getLocalstorageData('selectCategory');
        this.sortOnCategoryBase(this.selectedValue);
      } else {
        this.selectedValue = 'all';
        this.productService.setLocalstorageData('selectCategory', this.selectedValue);
      }
    }, error => {
      console.log('error');
      this.productData = [];
      this.productService.setLocalstorageData('productData', this.productData);
    });
  }

  onChange(deviceValue) {
    this.sortOnCategoryBase(deviceValue);
  }

  sortOnCategoryBase(selectCategory: string) {
    this.productData = this.productService.getProductData();

    if (selectCategory == "all") {
      this.productService.setLocalstorageData("selectCategory", selectCategory);
      this.productData = this.productData;
      this.dataSource = new MatTableDataSource<any>(this.productData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    } else {
      this.productService.setLocalstorageData("selectCategory", selectCategory);
      this.productData = this.productData.filter(function (e) {
        return e.category == selectCategory;
      });
      this.dataSource = new MatTableDataSource<any>(this.productData);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
}
