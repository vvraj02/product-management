export class Constant {
  public static readonly productCateoryList = [
    { key: 'cloth', value: 'Cloth' },
    { key: 'sport', value: 'Sport' },
    { key: 'mobile', value: 'Mobile' },
    { key: 'electric', value: 'Electric' }
  ];

  public static readonly PRODUCTDATA = [
    { id: '1644511541181', productName: 'MRF bat', description: 'MRF', price: 7000, category: "sport", mobile: '9367404845' },
    { id: '1644512133182', productName: 'Puma tshirt for mens', description: 'Puma', price: 1500, category: "cloth", mobile: '9367404745' },
    { id: '1644515524183', productName: 'Redmi note 10', description: 'redmi', price: 13000, category: "mobile", mobile: '9367464745' },
    { id: '1644515545184', productName: 'iphone 13', description: 'Iphone', price: 130000, category: "mobile", mobile: '9367404745' },
    { id: '1644515567181', productName: 'SG bat', description: 'SG bat', price: 6402, category: "sport", mobile: '9367444745' },
    { id: '1644515788182', productName: 'Adidas tshirt for mens black', description: 'Adidas', price: 3900, category: "cloth", mobile: '9367404745' },
    { id: '1644515456184', productName: 'Mobile cord', description: 'iphone', price: 899, category: "electric", mobile: '9367404725' },
    { id: '1644515347181', productName: 'Guru Tennis ball', description: 'Guru', price: 130, category: "sport", mobile: '9367404735' },
    { id: '1644515235182', productName: 'Cotton king white shirt', description: 'Shirt , white', price: 8704, category: "cloth", mobile: '9447404745' },
    { id: '1644515345184', productName: 'Headphone one plus', description: 'one plus', price: 2600, category: "electric", mobile: '9787404745' },
    { id: '1644515467181', productName: 'Puma shoose', description: 'Puma', price: 6000, category: "sport", mobile: '9367404747' },
    { id: '1644515356182', productName: 'White Peter England', description: 'peter england', price: 6400, category: "cloth", mobile: '9267404745' },
    { id: '1644515223184', productName: 'Extension board', description: '3 pins', price: 399, category: "electric", mobile: '9367403745' },
  ];
}


