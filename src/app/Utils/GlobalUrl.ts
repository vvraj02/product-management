//add here base url of backend 
const baseUrl = 'http://0.0.0.0:50004/api';
export class GlobalUrls {
   public static readonly addProductUrl = `${baseUrl}/add-product`;
   public static readonly getAllProductsUrl = `${baseUrl}/get-all-products`;
   public static readonly updateProductUrl = `${baseUrl}/update-product/{id}`;
   public static readonly deleteProductUrl = `${baseUrl}/product/{id}`;
}
