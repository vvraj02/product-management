import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductServiceService } from '../services/product-service.service';
import { ProductApisService } from '../services/product-apis.service'
import { Component, OnInit } from '@angular/core';
import { GlobalUrls } from './../Utils/GlobalUrl';
import { Constant } from '../models/constant.model';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  buttonName = 'Add Product';
  productForm: FormGroup;
  productCateoryList = [...Constant.productCateoryList]

  constructor(private productService: ProductServiceService, private productApisService: ProductApisService, private router: Router, private fb: FormBuilder, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.productForm = this.fb.group({
      id: new Date().valueOf().toString(),
      productName: ['', [Validators.required]],
      description: ['', [Validators.required]],
      category: ['cloth', Validators.required],
      price: ['', [Validators.required, Validators.pattern(/^[1-9]\d*(\.\d+)?$/im)]],
      mobile: ['', [Validators.required, Validators.pattern(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im)]],
    });

    const param = this.route.snapshot.params['id'];
    if (param) {
      this.setProductData(param);
    }
  }

  AddOrUpdateProduct() {
    if (this.buttonName === 'Update Product') {
      //using api call 
      // const payload = { ...this.productForm.value };
      // this.updateProduct(payload, payload.id.value);

      //using localstorage 
      this.productService.updateProduct(this.productForm.value);
      this.productService.openSnackBar('success', 'Product updated successfully');
    } else {
      //using api call 
      // const payload = { ...this.productForm.value };
      // this.addProduct(payload);

      //using localstorage 
      this.productService.addProduct(this.productForm.value);
      this.productService.openSnackBar('success', 'Product added successfully');
    }
    this.router.navigate(['/product-list']);
  }

  addProduct(payload) {
    this.productApisService.callPostApi(GlobalUrls.addProductUrl, payload).subscribe(response => {
      if (response) {
        this.router.navigate(['/product-list']);
      }
    }, error => {
      console.log('error', error);
    });
  }

  updateProduct(payload, id) {
    this.productApisService.callPutApi(GlobalUrls.updateProductUrl.replace('{id}', id), payload).subscribe(response => {
      if (response) {
        this.router.navigate(['/product-list']);
      }
    }, error => {
      console.log('error', error);
    });
  }

  setProductData(param) {
    const product = this.productService.getSingleProduct(param);
    this.buttonName = 'Update Product';
    this.productForm.controls.id.setValue(product.id);
    this.productForm.controls.productName.setValue(product.productName);
    this.productForm.controls.description.setValue(product.description);
    this.productForm.controls.category.setValue(product.category);
    this.productForm.controls.price.setValue(product.price);
    this.productForm.controls.mobile.setValue(product.mobile);
  }
}
