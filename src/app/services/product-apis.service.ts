import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../popup/snack-bar/snack-bar.component';
import { HttpClient, HttpParams } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductApisService {

  constructor(private snackBar: MatSnackBar, private http: HttpClient) { }

  callGetApi(apiUrl: string, reqParams?: HttpParams) {
    return this.http.get(apiUrl, { params: reqParams }).pipe(tap(
      data => this.handleResponse(data),
      error => this.handleErrorResponse(error, apiUrl)
    ));
  }

  callPostApi(apiUrl: string, body: any) {
    return this.http.post(apiUrl, body, { headers: {} }).pipe(
      tap(
        data => this.handleResponse(data),
        error => this.handleErrorResponse(error, apiUrl)
      ));
  }


  callPutApi(apiUrl: string, body: any, params?: HttpParams) {
    return this.http.put(apiUrl, body, { headers: {} }).pipe(
      tap(
        data => this.handleResponse(data),
        error => this.handleErrorResponse(error, apiUrl)
      )
    );
  }

  deletProduct(apiUrl: string) {
    return this.http.delete(apiUrl).pipe(tap(
      data => this.handleResponse(data),
      error => this.handleErrorResponse(error, apiUrl)
    ));
  }

  handleResponse(response) {
    if (response.statusCode === 200) {
      return response;
    }
  }

  handleErrorResponse(error, url?) {
    if (error.status === 401
      || error.statusCode === 401
      || error.response_data === 401) {
    } else {
      return error;
    }
  }
}
