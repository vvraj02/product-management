import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from '../popup/snack-bar/snack-bar.component';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Constant } from './../models/constant.model';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {

  constructor(private snackBar: MatSnackBar, private http: HttpClient) { }

  productData = [...Constant.PRODUCTDATA];

  getProductData() {
    if (!this.getLocalstorageData('selectCategory')) {
      this.setLocalstorageData('selectCategory', 'all');
    }
    if (this.getLocalstorageData('productData')) {
      return [...this.getLocalstorageData('productData')];
    }
    return [...this.productData];
  }


  addProduct(product) {
    product.id = Date.now().toString();
    this.productData.push(product);
    this.setLocalstorageData('productData', this.productData);
  }


  updateProduct(product) {
    const ProductData = this.getProductData();
    const productIndex = ProductData.findIndex(value => value.id === product.id);
    if (productIndex !== -1) {
      ProductData[productIndex] = product;
      this.setLocalstorageData('productData', ProductData);
    }
  }

  getLocalstorageData(key) {
    return JSON.parse(localStorage.getItem(key));
  }

  setLocalstorageData(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  deleteLocalstorageData(key) {
    localStorage.removeItem(key);
  }

  clearLocalstorageData() {
    localStorage.clear();
  }

  getSingleProduct(productId) {
    const productData = this.getProductData();
    const productObj = productData.find(value => value.id === productId);
    return productObj;

  }

  openSnackBar(value, msg, statusCode?): any {
    const durationInSeconds = 2;
    this.snackBar.openFromComponent(SnackBarComponent, {
      duration: durationInSeconds * 1000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      data: { message: msg, type: value, statusCode }
    });
  }

}
